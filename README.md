ffb - SUNFFB video driver for the Xorg X server
-----------------------------------------------

This driver supports Sun Creator, Creator 3D and Elite 3D video cards,
which are UPA bus devices for UltraSPARC workstations.

All questions regarding this software should be directed at the
Xorg mailing list:

  https://lists.x.org/mailman/listinfo/xorg

The primary development code repository can be found at:

  https://gitlab.freedesktop.org/xorg/driver/xf86-video-sunffb

Please submit bug reports and requests to merge patches there.

For patch submission instructions, see:

  https://www.x.org/wiki/Development/Documentation/SubmittingPatches
